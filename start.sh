#!/bin/bash

#prepare
rm -rf WEB-INF/classes
mkdir WEB-INF/classes

#compile and make war
./make_war.sh

#prepare docker image and run it
sudo docker-compose up --build
